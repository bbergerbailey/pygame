import pygame, sys
from pygame.locals import *
import random, time
pygame.init()

FPS = 60
FramePerSec = pygame.time.Clock()

BLUE = pygame.Color(0, 0, 255)
RED = pygame.Color(255, 0, 0)
GREEN = pygame.Color(0, 255, 0)
BLACK = pygame.Color(0, 0, 0)
WHITE = (255, 255, 255)

SCREEN_WIDTH = 400
SCREEN_HEIGHT = 600
SPEED = 5
SCORE = pygame.time.get_ticks()
highscore = ""

with open('highscore.txt', 'r') as f:
    try:
        highestscore = int(f.read())
    except:
        highestscore = 0




#Fonts
font = pygame.font.SysFont("comicsans", 60)
font_small = pygame.font.SysFont("comicsans", 20)
game_over = font.render("GAME OVER", True, BLACK)
play_again = font_small.render("PLAY AGAIN? y or n", True, BLACK)





background= pygame.image.load("AnimatedStreet.png")

#create a white screem
DISPLAYSURF = pygame.display.set_mode((400,600))
DISPLAYSURF.fill(WHITE)
pygame.display.set_caption("Game")

class Enemy(pygame.sprite.Sprite):
    def __init__(self):
        super().__init__()
        self.image = pygame.image.load("Enemy.png")
        self.rect = self.image.get_rect()
        self.rect.center=(random.randint(40,SCREEN_WIDTH-40), 0)

    def move(self):
        self.rect.move_ip(0,SPEED)
        if (self.rect.bottom > 600):
            self.rect.top = 0
            self.rect.center = (random.randint(30, 370), 0)

      #def draw(self, surface):
       # surface.blit(self.image, self.rect)


class Player(pygame.sprite.Sprite):
    def __init__(self):
        super().__init__()
        self.image = pygame.image.load("Player.png")
        self.rect = self.image.get_rect()
        self.rect.center = (160, 520)

    def move(self):
        pressed_keys = pygame.key.get_pressed()
       #if pressed_keys[K_UP]:
            #self.rect.move_ip(0, -5)
       #if pressed_keys[K_DOWN]:
            #self.rect.move_ip(0,5)

        if self.rect.left > 0:
              if pressed_keys[K_LEFT]:
                  self.rect.move_ip(-5, 0)
        if self.rect.right < SCREEN_WIDTH:
              if pressed_keys[K_RIGHT]:
                  self.rect.move_ip(5, 0)

    #def draw(self, surface):
       # surface.blit(self.image, self.rect)

def game_init():
    P1 = Player()
    E1 = Enemy()
    SCORE = pygame.time.get_ticks()



    #Creating Sprites Groups
    enemies = pygame.sprite.Group()
    enemies.add(E1)
    all_sprites = pygame.sprite.Group()
    all_sprites.add(P1)
    all_sprites.add(E1)

    #Adding a new User event
    INC_SPEED = pygame.USEREVENT + 1
    pygame.time.set_timer(INC_SPEED, 1000)

    return [all_sprites, P1, enemies, INC_SPEED, SCORE]



    #Game Loop
def game_exec(all_sprites, P1, enemies, INC_SPEED, SCORE, SPEED):
    running = True
    while running:


        #Cycles through all events occuring
        for event in pygame.event.get():
            if event.type == INC_SPEED:
                SPEED += 0.5

            if event.type == QUIT:
                pygame.quit()
                sys.exit()

        DISPLAYSURF.blit(background, (0, 0))
        counting_time = pygame.time.get_ticks() - SCORE
        counting_minutes = str(counting_time//60000).zfill(2)
        counting_seconds = str( (counting_time%60000)//1000 ).zfill(2)
        counting_millisecond = str((counting_time%1000)//10).zfill(2)
        counting_string = "%s:%s:%s" % (counting_minutes, counting_seconds, counting_millisecond)
        scores = font_small.render(counting_string, True, BLACK)
        DISPLAYSURF.blit(scores, (10,10))




        #Moves and Re-draws all Sprites
        for entity in all_sprites:
            DISPLAYSURF.blit(entity.image, entity.rect)
            entity.move()

        #To be run if collision occurs between Player and Enemy
        if pygame.sprite.spritecollideany(P1, enemies):
            pygame.mixer.Sound('crash.wav').play()
            time.sleep(0.5)
            pygame.display.update()
            for entity in all_sprites:
                entity.kill()
                #time.sleep(2)
            running = False
            global highscore
            global highestscore
            if counting_time > highestscore:
                highestscore = counting_time
                with open('highscore.txt', 'w') as f:
                    f.write(str(highestscore))
            counting_high_minutes = str(highestscore//60000).zfill(2)
            counting_high_seconds = str( (highestscore%60000)//1000 ).zfill(2)
            counting_high_millisecond = str((highestscore%1000)//10).zfill(2)
            highscore = "%s:%s:%s" % (counting_high_minutes, counting_high_seconds, counting_high_millisecond)

            return counting_string




        pygame.display.update()
        FramePerSec.tick(FPS)



gameover = False
while not gameover:
    start = game_init()
    finalscore = game_exec(*start, SPEED)
    DISPLAYSURF.fill(RED)
    final_score = font_small.render(f"SCORE: {finalscore} ", True, BLACK)
    high_score = font_small.render(f"HIGHSCORE: {highscore} ", True, BLACK)
    DISPLAYSURF.blit(high_score,(90, 200))
    DISPLAYSURF.blit(game_over,(20, 250))
    DISPLAYSURF.blit(final_score,(115, 360))
    DISPLAYSURF.blit(play_again,(95, 410))
    pygame.display.update()
    choosing = True
    while choosing:
        for event in pygame.event.get():
            if event.type == KEYDOWN:
                if event.key == K_n:
                    gameover = True
                    pygame.quit()
                    sys.exit()
                elif event.key == K_y:
                    choosing = False
