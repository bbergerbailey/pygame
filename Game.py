#Imports
import mygame, sys
from pygame.locals import *
import random, time

#Initialzing
mygame.init()

#Setting up FPS
FPS = 60
FramePerSec = mygame.time.Clock()

#Creating colors
BLUE  = (0, 0, 255)
RED   = (255, 0, 0)
GREEN = (0, 255, 0)
BLACK = (0, 0, 0)
WHITE = (255, 255, 255)

#Other Variables for use in the program
SCREEN_WIDTH = 400
SCREEN_HEIGHT = 600
SPEED = 5
SCORE = 0

#Setting up Fonts
font = mygame.font.SysFont("Verdana", 60)
font_small = mygame.font.SysFont("Verdana", 20)
game_over = font.render("Game Over", True, BLACK)

background = mygame.image.load("AnimatedStreet.png")

#Create a white screen
DISPLAYSURF = mygame.display.set_mode((400,600))
DISPLAYSURF.fill(WHITE)
mygame.display.set_caption("Game")


class Enemy(mygame.sprite.Sprite):
      def __init__(self):
        super().__init__()
        self.image = mygame.image.load("Enemy.png")
        self.rect = self.image.get_rect()
        self.rect.center = (random.randint(40,SCREEN_WIDTH-40), 0)

      def move(self):
        global SCORE
        self.rect.move_ip(0,SPEED)
        if (self.rect.bottom > 600):
            SCORE += 1
            self.rect.top = 0
            self.rect.center = (random.randint(40, SCREEN_WIDTH - 40), 0)


class Player(mygame.sprite.Sprite):
    def __init__(self):
        super().__init__()
        self.image = mygame.image.load("Player.png")
        self.rect = self.image.get_rect()
        self.rect.center = (160, 520)

    def move(self):
        pressed_keys = mygame.key.get_pressed()

        if self.rect.left > 0:
              if pressed_keys[K_LEFT]:
                  self.rect.move_ip(-5, 0)
        if self.rect.right < SCREEN_WIDTH:
              if pressed_keys[K_RIGHT]:
                  self.rect.move_ip(5, 0)


#Setting up Sprites
P1 = Player()
E1 = Enemy()

#Creating Sprites Groups
enemies = mygame.sprite.Group()
enemies.add(E1)
all_sprites = mygame.sprite.Group()
all_sprites.add(P1)
all_sprites.add(E1)

#Adding a new User event
INC_SPEED = mygame.USEREVENT + 1
mygame.time.set_timer(INC_SPEED, 1000)

#Game Loop
while True:

    #Cycles through all events occuring
    for event in mygame.event.get():
        if event.type == INC_SPEED:
              SPEED += 0.5
        if event.type == QUIT:
            mygame.quit()
            sys.exit()


    DISPLAYSURF.blit(background, (0,0))
    scores = font_small.render(str(SCORE), True, BLACK)
    DISPLAYSURF.blit(scores, (10,10))

    #Moves and Re-draws all Sprites
    for entity in all_sprites:
        entity.move()
        DISPLAYSURF.blit(entity.image, entity.rect)


    #To be run if collision occurs between Player and Enemy
    if mygame.sprite.spritecollideany(P1, enemies):
          mygame.mixer.Sound('crash.wav').play()
          time.sleep(1)

          DISPLAYSURF.fill(RED)
          DISPLAYSURF.blit(game_over, (30,250))

          mygame.display.update()
          for entity in all_sprites:
                entity.kill()
          time.sleep(2)
          mygame.quit()
          sys.exit()

    mygame.display.update()
    FramePerSec.tick(FPS)
